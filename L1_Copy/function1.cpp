#include <iostream>
using namespace std;

double something(double a, double b, char operand)
{
    if (operand == '+')
    {
        double result = a + b;
        return result;
    }
    else if (operand == '-')
    {
        double result = a - b;
        return result;
    }
    else if (operand == '*')
    {
        double result = a * b;
        return result;
    }
    else if (operand == '/')
    {
        double result = a / b;
        return result;
    }

}

int main()
{
    double a, b;
    char op;
    cin >> a >> b >> op;
    cout << "Result : " <<something(a, b, op) << endl;
    return 0;
}
