#include<iostream>

using namespace std;

bool is_prime(int number)
{
    bool prime = true;
    int counter = 0;
    for(int i = 2; i < number; i ++)
    {
        if(number % i == 0)
        {
            counter++;
            break;
        }
    }
    if(counter == 0)
    {
        prime = true;
    }
    else
    {
        prime = false;
    }
    return prime;
}
int main()
{
    bool prime = false;
    int number;
    while ( cin >> number)
    {
        if(number < 2)
        {
            cout << "N/A" << endl;
        }
        else
        {
            prime = is_prime(number);
            if(prime == true)
            {
                cout << "YES" << endl;
            }
            else
            {
                cout << "NO" << endl;
            }
        }
    }
    cout << "Ne razbiram vhoda" << endl;
    return 0;
}
