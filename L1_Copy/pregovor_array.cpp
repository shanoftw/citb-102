#include<iostream>

using namespace std;

bool is_good(int number)
{
    bool good = true;
    while(number > 0)
    {
        if((number % 10) % 2 == 0)
        {
            good = true;
        }
        else
        {
            good = false;
            break;
        }
        number = number / 10;
    }
    return good;
}
int main()
{
    bool good = false;
    int number;
    while ( cin >> number)
    {
        if(number < 2)
        {
            cout << "N/A" << endl;
        }
        else
        {
            good = is_good(number);
            if(good == true)
            {
                cout << "YES" << endl;
            }
            else
            {
                cout << "NO" << endl;
            }
        }
    }
    return 0;
}
