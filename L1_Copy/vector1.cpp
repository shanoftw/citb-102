#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
double SumValue(vector<double> A)
{
    double sum = 0;
    for (int i = 0;i < A.size(); i++)
    {
        sum += A[i];
    }
    return sum;
}
double AvgValue(vector<double> A)
{
    return SumValue(A) / A.size();
}
double MinValue(vector<double> A)
{
    double result = A[0];
    for (int i = 1;i < A.size(); i++)
    {
        result = min (A[i], result);
    }
    return result;
}
double MaxValue(vector<double> A)
{
    double result = A[0];
    for (int i = 1;i < A.size(); i++)
    {
        result = max (A[i], result);
    }
    return result;
}

int main()
{
    int broi;
    string command;
    cin >> broi;
    vector<double> A(broi);
    for (int i = 0; i < A.size(); i ++)
    {
        cin >> A[i];
    }
    while (cin >> command)
    {
        if (command == "sum")
        {
            cout << SumValue(A) << endl;
        }
        else if (command == "avg")
        {
            cout << AvgValue(A) << endl;
        }
        else if (command == "min")
        {
            cout << MinValue(A) << endl;
        }
        else if (command == "max")
        {
            cout << MaxValue(A) << endl;
        }
        else if (command == "ins")
        {
            double ins_num = 0;
            cin >> ins_num;
            A.push_back(ins_num);
        }
        else if (command == "del")
        {
            double num = 0;
            cin >> num;
            num -= 1;
            A.erase (A.begin()+num);
        }
    }
    return 0;
}
