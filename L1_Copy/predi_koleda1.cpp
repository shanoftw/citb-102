#include <iostream>
using namespace std;
const int NOT_SET = -1;
double eurbgn = NOT_SET, eurusd = NOT_SET, usdbgn = NOT_SET, usdeur = NOT_SET, bgneur = NOT_SET, bgnusd = NOT_SET;
double& getRateFor(string str1, string str2)
{
    if(str1 == "EUR" && str2 == "BGN")
    {
        return eurbgn;
    }
    else if(str1 == "EUR" && str2 == "USD")
    {
        return eurusd;
    }
    else if(str1 == "USD" && str2 == "BGN")
    {
        return usdbgn;
    }
    else if(str1 == "USD" && str2 == "EUR")
    {
        return usdeur;
    }
    else if(str1 == "BGN" && str2 == "EUR")
    {
        return bgneur;
    }
    else if(str1 == "BGN" && str2 == "USD")
    {
        return bgnusd;
    }
}
void set_rate(string str1, string str2, double kurs)
{
    double& rate = getRateFor(str1, str2);
    rate = kurs;
}
double rate(string str1, string str2)
{
    double rate = getRateFor(str1, str2);
    return rate;
}
double convert(string str1, string str2, double ammount)
{
    double rate = getRateFor(str1, str2);
    return ammount * rate;
}
int main()
{
    string op, c1, c2;
    double r;
    while(cin >> op)
    {
        if(op == "set")
        {
            cin >> c1 >> c2 >> r;
            set_rate(c1, c2, r);
        }
        else if(op == "rate")
        {
            if (rate(c1, c2) == -1)
            {
                cout << "N/A." << endl;
            }

            else
            {
                cin >> c1 >> c2;
                cout << rate(c1, c2) << endl;
            }
        }
        else if(op == "convert")
        {
            if (rate(c1, c2) == -1)
            {
                cout << "N/A." << endl;
            }
            else
            {
                double ammount;
                cin >> c1 >> c2 >> ammount;
                cout << convert(c1, c2, ammount) << endl;
            }
        }
        else
        {
            break;
        }
    }
    return 0;
}
//proverkata za N/A. trqbva da se opravi
