#include <iostream>
#include <string>
using namespace std;

void in_reverse(string& text)
{
    text = string(text.rbegin(), text.rend());
}
int main()
{
    string text;
    while(getline(cin, text))
    if (text == "exit")
    {
        break;
    }
    else
    {
        in_reverse(text);
        cout << text << endl;
    }
    return 0;
}
