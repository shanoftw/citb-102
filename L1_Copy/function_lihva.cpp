#include <iostream>
using namespace std;

double prosta(double cena, double interest, double godini)
{
    cena *= 1 + (godini * ( interest / 100 ) );
    return cena;
}
double slojna(double cena, double interest, double godini)
{
    cena *= 1 + ( interest / 100 );
    return cena;
}

int main()
{
    double cena, interest, godini;
    cin >> cena >> interest >> godini;
    cout << "Prosta : " <<prosta(cena, interest, godini) << endl << "Slojna : " <<slojna(cena, interest, godini) << endl;
    return 0;
}
